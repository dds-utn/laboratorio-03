package ar.edu.utn.dds;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.security.spec.ECField;
import java.util.*;
import java.util.stream.Collectors;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;


public class MiServer {
    /**
     * http://sparkjava.com/tutorials/reducing-java-boilerplate
     */

    private static List<Premio> premios = new ArrayList<>();

    public static void loadPremios() {


        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new StringReader(
                    IOUtils.toString(MiServer.class.getClassLoader().getResourceAsStream("nobel.csv"))));
            int idx = 0;
            while ((line = br.readLine()) != null) {
                idx += 1;
                if (idx == 1) {
                    continue;
                } else {

                    String[] fields = line.split(cvsSplitBy);
                    try{
                    premios.add(new Premio(idx, Integer.parseInt(fields[0]),
                            fields[1], fields[2], fields[3], fields[4], fields[5],
                            fields[6], fields[7], fields[8], fields[9], fields[10]

                    ));
                    } catch(NumberFormatException es){}
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static List<Premio> listPremios(Request req, Response res) {
        if (req.queryMap().hasKey("country")){
            return premios.stream().filter(
                    premio -> premio.getCounty().equals(req.queryMap("country").value()))
                    .collect(Collectors.toList());
        }
        return premios;
    }

    public static ApiMsg addPremio(Request req, Response res) {
        Premio newPremio = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            newPremio = mapper.readValue(req.body(), Premio.class);
        } catch (JsonParseException e) {
            //res.status(?);
            //return new ApiMsg("??", e.getLocalizedMessage());
        } catch (JsonMappingException e) {
            //res.status(?);
            //return new ApiMsg("??", e.getLocalizedMessage());
        } catch (IOException e) {
            //res.status(?);
            //return new ApiMsg("??", e.getLocalizedMessage());
        }
        final Premio newPremio2 = newPremio;
        Optional<Premio> first = premios.stream().filter(Premio -> Premio.equals(newPremio2)).findFirst();
        if (first.isPresent()) {
            //res.status(?);
            //return new ApiMsg(?, ?);
        }
        OptionalInt id = premios.stream().mapToInt(Premio -> Premio.getId()).max();
        newPremio2.setId(id.getAsInt() + 1);
        premios.add(newPremio2);
        //res.status(?);
        return new ApiMsg("?", "new id = " + newPremio2.getId().toString());
    }

    public static void main(String[] args) {
        enableDebugScreen();
        port(4567);

        loadPremios();

        get("/premios", MiServer::listPremios, new JsonTransformer());

        post("/premios", MiServer::addPremio, new JsonTransformer());


        get("/premios/:id", (req, res) -> {
                    Optional<Premio> optionalPremio = premios.stream().filter(premio ->
                            premio.getId().toString().equals(req.params("id").toString())).findFirst();
                    if (optionalPremio.isPresent()) {
                        return optionalPremio.get();
                    }
                    res.status(404);
                    return new ApiMsg("not found", "code " + req.params("id").toString() + " not found");

                },
                new JsonTransformer()
        );

        delete("/premios/:id", (req, res) -> {

            if (Integer.parseInt(req.params("id").toString()) <= 8) {
                res.status(403);
                return new ApiMsg("delete forbiden", "cant delete default premio (id < 8)");
            }

            Optional<Premio> optionalPremio = premios.stream().filter(premio -> (
                    premio.getId().toString().equals(req.params("id").toString()))).findFirst();
            if (optionalPremio.isPresent()) {
                premios.remove(optionalPremio.get());
                return new ApiMsg("premio deleted", optionalPremio.get().toString());
            } else {
                res.status(404);
                return new ApiMsg("not found", "id " +
                        req.params("id").toString() + " was not found");
            }

        }, new JsonTransformer());


    }

}
