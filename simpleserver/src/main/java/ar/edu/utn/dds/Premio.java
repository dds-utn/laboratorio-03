package ar.edu.utn.dds;

public class Premio {

    private Integer id;
    private Integer year;
    private String category;
    private String name;
    private String birthdate;
    private String birthPlace;
    private String county;
    private String residence;
    private String role;
    private String field;
    private String prize;
    private String motivation;

    public Premio(Integer id, Integer year,
                  String category, String name, String birthdate, String birthPlace, String county,
                  String residence, String role, String field, String prize, String motivation) {
        this.id = id;
        this.year = year;
        this.category = category;
        this.name = name;
        this.birthdate = birthdate;
        this.birthPlace = birthPlace;
        this.county = county;
        this.residence = residence;
        this.role = role;
        this.field = field;
        this.prize = prize;
        this.motivation = motivation;
    }

    public Premio() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }
}
