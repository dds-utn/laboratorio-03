package ar.edu.utn.dds;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

public class Auto {
    private String modelo;
    private String marca;
    private int anio;
    private int id;

    @JsonIgnore
    private boolean activo;


    public Auto() {
        this.activo = true;
    }

    public Auto(int id, String modelo, String marca, int anio) {
        this.id = id;
        this.modelo = modelo;
        this.marca = marca;
        this.anio = anio;
        this.activo = true;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auto auto = (Auto) o;
        return anio == auto.anio &&
                Objects.equals(modelo, auto.modelo) &&
                Objects.equals(marca, auto.marca);
    }

    @Override
    public int hashCode() {

        return Objects.hash(modelo, marca, anio);
    }

    @Override
    public String toString() {
        return "Auto{" +
                "modelo='" + modelo + '\'' +
                ", marca='" + marca + '\'' +
                ", anio=" + anio +
                '}';
    }
}
