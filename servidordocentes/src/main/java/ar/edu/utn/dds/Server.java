package ar.edu.utn.dds;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;
import java.util.stream.Collectors;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;


public class Server {
    /**
     * http://sparkjava.com/tutorials/reducing-java-boilerplate
     */


    public static void main(String[] args) {
        enableDebugScreen();
        port(4567);

        List<Auto> autos = new ArrayList<>( Arrays.asList(new Auto[]{
                new Auto(1, "Astra", "Chevrolet", 2008),
                new Auto(2, "Astra", "Chevrolet", 2007),
                new Auto(3, "Corsa", "Chevrolet", 2018),
                new Auto(4, "Corsa", "Chevrolet", 2012),
                new Auto(5, "Siena", "Fiat", 2018),
                new Auto(6, "Siena", "Fiat", 2017),
                new Auto(7, "Punto", "Fiat", 2015),
                new Auto(8, "Punto", "Fiat", 2016)
        }));

        get("/autos", (req, res) -> autos, new JsonTransformer());
        get("/autos/:id", (req, res) -> {
                    Optional<Auto> optionalAuto = autos.stream().filter( auto -> 
                            auto.getId().toString().equals(req.params("id").toString())).findFirst();
                    if (optionalAuto.isPresent()) {
                        return optionalAuto.get();
                    }
                    res.status(404);
                    return new ApiMsg("not found", "code " + req.params("id").toString() + " not found");

                },
                new JsonTransformer()
        );

        delete("/autos/:id",(req, res) -> {

            if( Integer.parseInt(req.params("id").toString()) <= 8){
                res.status(403);
                return new ApiMsg("delete forbiden","cant delete default cars (id < 8)");
            }

            Optional<Auto> optionalAuto = autos.stream().filter(auto -> auto.isActivo() && (
                    auto.getId().toString().equals(req.params("id").toString()))).findFirst();
            if(optionalAuto.isPresent()){
                autos.remove(optionalAuto.get());
                return new ApiMsg("car deleted",optionalAuto.get().toString());
            }  else{
                res.status(404);
                return new ApiMsg("not found","id " +
                        req.params("id").toString() + " was not found");
            }

        },new JsonTransformer());

        post("/autos", (req, res) -> {
            Auto newAuto = null;
            try {
                ObjectMapper mapper = new ObjectMapper();
                newAuto = mapper.readValue(req.body(), Auto.class);
            } catch (JsonParseException e) {
                res.status(400);
                return new ApiMsg("json bad formed", e.getLocalizedMessage());
            }
            final Auto newAuto2 = newAuto;
            Optional<Auto> first = autos.stream().filter(auto -> auto.equals(newAuto2)).findFirst();
            if(first.isPresent()){
                res.status(422);
                return new ApiMsg("repeated", "already exists: " + newAuto2.toString() );
            }
            OptionalInt id = autos.stream().mapToInt(auto -> auto.getId()).max()  ;
            newAuto2.setId(id.getAsInt() + 1);
            autos.add(newAuto2);
            res.status(201);
            return new ApiMsg("created","new id = " + newAuto2.getId().toString());

        },new JsonTransformer());
    }

}
