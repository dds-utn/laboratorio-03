package ar.edu.utn.dds;

import java.io.ByteArrayOutputStream;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

public class RESTClient {

	public static void main(String[] args) {
		HttpResponse<String> response;
		try {
			String keggUrl = "http://localhost:4567/premios";
			response = Unirest.get(keggUrl).asString();
			if (response.getStatus() == 200) {

				ByteArrayOutputStream result = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int length;
				while ((length = response.getRawBody().read(buffer)) != -1) {
					result.write(buffer, 0, length);
				}
				// StandardCharsets.UTF_8.name() > JDK 7
				String output = result.toString("UTF-8");
				System.out.println(output);

			} else {
				System.out.println(response.getStatusText() + " -- " + response.getBody());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
